from django import forms
from .models import Contact


class ContactUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ContactUpdateForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Contact
        fields = ('first_name', 'last_name', 'customer_email', 'customer_number', 'user')

    def clean(self):
        clean_data = super(ContactUpdateForm, self).clean()

        if clean_data['user'] == None:
            clean_data['user'] = self.request.user

            return clean_data


