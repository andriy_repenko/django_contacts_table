from django.urls import path, re_path
from . import views


app_name = "contacts"
urlpatterns = [
    path("", views.ContactsPageView.as_view() , name= "contacts" ),
    path("?page=n", views.ContactsPageView.as_view(), name= "contacts"),
    path("~create/", views.ViewCreatePost.as_view(), name="create"),
    re_path(r"^update/(?P<pk>\d+)/$", views.ViewUpdatePost.as_view(), name="update"),
    re_path(r"^delete/(?P<pk>\d+)/$", views.ViewDeletePost.as_view(), name="delete")
]
