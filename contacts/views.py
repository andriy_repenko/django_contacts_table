from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Contact
from django.views.generic import CreateView, UpdateView, DeleteView, ListView
from django.urls import reverse_lazy
from .forms import ContactForm
from .form_for_update import ContactUpdateForm
from django.core.paginator import Paginator
from django.shortcuts import render



class ContactsPageView(LoginRequiredMixin, ListView):
    template_name = 'account/contacts.html'
    context_object_name = "contacts_list"
    paginate_by = 5

    def get_queryset(self):
        queryset = self.request.user.contacts.all()
        return queryset


class ViewCreatePost(LoginRequiredMixin, CreateView):
    template_name = 'account/create.html'
    form_class = ContactForm
    success_url = reverse_lazy('contacts:contacts')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(ViewCreatePost, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(ViewCreatePost, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs


class ViewUpdatePost(LoginRequiredMixin, UpdateView):
    model = Contact
    template_name = 'account/update.html'
    form_class = ContactUpdateForm
    success_url = reverse_lazy('contacts:contacts')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(ViewUpdatePost, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(ViewUpdatePost, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs


class ViewDeletePost(LoginRequiredMixin, DeleteView):
    template_name = 'account/delete.html'
    model = Contact
    success_url = reverse_lazy('contacts:contacts')
