from django.db import models
from django.contrib.auth.models import User


class Contact(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    customer_email = models.EmailField(blank=False, null=False)
    customer_number = models.CharField(max_length=16)
    user = models.ForeignKey('users.User', on_delete=models.SET_NULL, null=True, blank=True, related_name='contacts')

    def __str__(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    class Meta:
        unique_together = (("user", "customer_number"), ("user", "customer_email"))
