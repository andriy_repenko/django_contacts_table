# Generated by Django 2.0.10 on 2019-02-22 14:15

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contacts', '0008_auto_20190222_1520'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='contact',
            unique_together={('user', 'customer_number')},
        ),
    ]
