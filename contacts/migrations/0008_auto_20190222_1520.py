# Generated by Django 2.0.10 on 2019-02-22 13:20

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contacts', '0007_auto_20190221_2039'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='contact',
            unique_together={('user', 'customer_number'), ('user', 'customer_email')},
        ),
    ]
