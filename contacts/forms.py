from django import forms
from .models import Contact


class ContactForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ContactForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Contact
        fields = ('first_name', 'last_name', 'customer_email', 'customer_number')

    def clean(self):
        clean_data = super(ContactForm, self).clean()
        customer_email = clean_data.get('customer_email')
        customer_number = clean_data.get('customer_number')

        if Contact.objects.filter(customer_email=customer_email, user=self.request.user).exists():
            msg = u'This email already exsist.'
            self._errors['customer_email'] = self.error_class([msg])

        if Contact.objects.filter(customer_number=customer_number, user=self.request.user).exists():
            msg = u'This number already exsist.'
            self._errors['customer_number'] = self.error_class([msg])
        return clean_data

